// Bibliotecas
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

// Função Main
int main() {
 setlocale(LC_ALL, "Portuguese"); // Comando responsável por dedixar o programa em português Brasil
 int n; // Número de entrada
 int r; // Resultado do deslocamento
 int i; // Contador
 char opcao; // Variável responsável pela repetição do programa

 // Comando inicial para repetição  
  do{
 system("cls");
 fflush(stdin);
 system("color 0F");
 
 // Lê o número
 printf("\n Digite o número (Com no máximo 10 Numerais): ");
 scanf("%d", &n);
 
 // Comando para limpar tela e dados existentes
 system("cls");
 fflush(stdin);
 
 // Menssagem para deixar o resultado maiss visual
 printf("\n O número em Decimal de %d em Binário é ", n);
 
 // Utiliza um número de 32 bits como base para a conversão.
 for(i = 31; i >= 0; i--) {
 	
    // Executa a operação shift right até a 
    // Última posição da direita para cada bit.
    r = n >> i;
    
    // Por meio do "e" lógico ele compara se o valor 
    // Na posição mais à direita é 1 ou 0 
    // E imprime na tela até reproduzir o número binário.
    if(r & 1) {
    	
        printf("1");
        
    } else {
    	
        printf("0");
    }
 }
 
  // Comando para limpar dados existentes 
 	fflush(stdin);
 	
  // Mistura de Cores para dar um efeito visual legal
 	system("color 01");
	system("color 02");
	system("color 03");
	system("color 04");
	system("color 05");
	system("color 06");
	system("color 07");
	system("color 08");
	system("color 08");
	system("color 09");
	system("color 0A");
	system("color 0B");
	system("color 0C");
	system("color 0D");
	system("color 0E");
	system("color 0F");
 	system("color 01");
	system("color 02");
	system("color 03");
	system("color 04");
	system("color 05");
	system("color 06");
	system("color 07");
	system("color 08");
	system("color 08");
	system("color 09");
	system("color 0A");
	system("color 0B");
	system("color 0C");
	system("color 0D");
	system("color 0E");
	system("color 0F");
	system("color 02");
	system("color 03");
	system("color 04");
	system("color 05");
	system("color 06");
	system("color 07");
	system("color 08");
	system("color 08");
	system("color 09");
	system("color 0A");
	system("color 0B");
	system("color 0C");
	system("color 0D");
	system("color 0E");
	system("color 0F");
	system("color 02");
	system("color 03");
	system("color 04");
	system("color 05");
	system("color 06");
	system("color 07");
	system("color 08");
	system("color 08");
	system("color 09");
	system("color 0A");
	system("color 0B");
	system("color 0C");
	system("color 0D");
	system("color 0E");
	system("color 0F");
	system("color 02");
	system("color 03");
	system("color 04");
	system("color 05");
	system("color 06");
	system("color 07");
	system("color 08");
	system("color 08");
	system("color 09");
	system("color 0A");
	system("color 0B");
	system("color 0C");
	system("color 0D");
	system("color 0E");
	system("color 0F");
	system("color 02");
	system("color 03");
	system("color 04");
	system("color 05");
	system("color 06");
	system("color 07");
	system("color 08");
	system("color 08");
	system("color 09");
	system("color 0A");
	system("color 0B");
	system("color 0C");
	system("color 0D");
	system("color 0E");
	system("color 0F");
	system("color 03");
	system("color 04");
	system("color 05");
	system("color 06");
	system("color 07");
	system("color 08");
	system("color 08");
	system("color 09");
	system("color 0A");
	system("color 0B");
	system("color 0C");
	system("color 0D");
	system("color 0E");
	system("color 0F");
	system("color 02");
	system("color 03");
	system("color 04");
	system("color 05");
	system("color 06");
	system("color 07");
	system("color 08");
	system("color 08");
	system("color 09");
	system("color 0A");
	system("color 0B");
	system("color 0C");
	system("color 0D");
	system("color 0E");
	system("color 0F");
	
 // Comando inicial para a repetição do programa
    printf("\n\n\n Deseja continuar no programa (S ou N)? ");
    fflush(stdin);
    scanf("%c", &opcao);

    }while(opcao == 'S' || opcao == 's');

    return 0;
}
