# Descrição do Projeto

Nesse projeto irei apresentar um programa em c, com uma função de transformar números decimais em números binários com 32 numerais, para que seja possível sua conversão sem falhas.

# Introdução

Sistema Decimal é um sistema de numeração posicional que representa os números em base 10, utiliza portanto dez símbolos diferentes. A palavra decimal tem origem no latim, decem que significa precisamente dez. Os especialistas e historiadores são unânimes, em considerar que este modo de contar em base 10, se deve aos dez dedos que temos nas mãos. Este sistema utiliza os símbolos 0, 1, 2, 3, 4, 5, 6, 7, 8 e 9. Estes algarismos são chamados de indo-arábico porque tiveram origem nos trabalhos iniciados pelos hindus e pelos árabes. No sistema decimal, cada conjunto de dez unidades forma uma nova ordem. Por exemplo, dez unidades equivalem a uma dezena; dez dezenas equivalem a uma centena; dez centenas equivale a um milhar e assim por diante.

O Sistema Binário é um sistema de numeração posicional que representa os números em base 2, utiliza portanto dois símbolos diferentes. Este sistema utiliza os símbolos 0 e 1. O sistema binário é o sistema mais utilizado por computadores e restantes aparelhos similares, isto porque os sistemas digitais trabalham internamente com dois estados (verdadeiro ou falso, ligado ou desligado). De acordo com alguns manuscritos antigos, o sistema binário já era utilizado na China 3000 anos antes de Cristo. Apesar deste sistema ser muito utilizado por todos os aparelhos eletrónicos, ele possui um grande inconveniente em relação ao sistema decimal ou hexadecimal. A representação de números no sistema binário ocupa muito espaço. Por exemplo o número 900.000 que na base 10 pode ser escrito com seis algarismos, na base 2 são necessários vinte algarismos.

## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9868812/avatar.png?width=400)  | Matheus Oliveira | [@Mathsto19](https://gitlab.com/Mathsto19) | [Matheusaugustooliveira@alunos.utfpr.edu.br](mailto:Matheusaugustooliveira@alunos.utfpr.edu.br)





